module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-jade');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.initConfig({
        less: {
            development: {
                files: {
                    "style/style.css": "style/style.less"
                }
            }
        },
        jade: {
            compile: {
                options: {
                    pretty: true
                },
                files: {
                    "index.html": ["index.jade"],
                    "play.html": ["play.jade"]
                }
            }
        },
        connect: {
            server: {
                options: {
                    livereload: true,
                    base: '.'
                }
            }
        },
        watch: {
            options: {
                livereload: true
            },
            styles: {
                files: ['**/*.less'],
                tasks: ['less'],
                options: {
                    spawn: false
                }
            },
            jade: {
                files: ['**/*.jade'],
                tasks: ['jade'],
                options: {
                    spawn: false
                }
            }
        }
    });

    grunt.registerTask('default', ['less', 'jade', 'connect:server', 'watch']);
};
