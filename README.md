# How to run a lab #
Install npm for your system [here](https://nodejs.org/).
Then install Grunt:
```
npm install -g grunt-cli
```
Go to the directory repository and run:
```
npm install
```
Then you can start grunt and open a lab in your browser (localhost:8000)
```
grunt
```