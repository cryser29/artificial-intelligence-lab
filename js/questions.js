$(function () {
    var root = $('.tab.questions > .column');
    var addBtn = root.find('.header > .add');
    var container = root.find('.items');

    addBtn.click(function () {
        var view = createEditableView({text: '', path: []});
        container.append(view);
    });

    updateQuestions();

    function updateQuestions() {
        container.empty();
        $.each(Base().getQuestions(), function (i, q) {
            container.append(createStaticView(q, i));
        });
    }

    function createStaticView(q, index) {
        var path = stringifyPath(q.path);

        var deleteBtn = $('<div/>').addClass('delete').click(function () {
            if (confirm('Вы уверены, что хотите удалить вопрос?')) {
                Base().deleteQuestion(index);
                updateQuestions();
            }
        });

        var editBtn = $('<div/>').addClass('edit').click(function () {
            result.replaceWith(createEditableView(q, index));
        });

        var result = $('<div/>').addClass('question-static')
            .append($('<div/>').text(q.text))
            .append($('<div/>').text(path))
            .append(deleteBtn)
            .append(editBtn);

        return result;
    }

    function createEditableView(q, index) {
        var result = $('<div/>').addClass('editable');
        var text = $('<input>').attr('type', 'text').val(q.text);
        var selects = [];

        if (q.path.length)
            for (var i = 0; i < q.path.length; i++) {
                selects.push(createObjectSelect(q.path.slice(0, i), q.path[i]));
            }
        else
            selects.push(createObjectSelect([], null));

        var acceptBtn = $('<button/>').addClass('icon accept').click(function () {
            var q = {text: text.val(), path: []};
            result.find('select').each(function (i, select) {
                var value = $(select).val();
                if (value)
                    q.path.push(value);
            });

            if (!q.text) {
                alert('Тело вопроса не должно быть пустым!');
                return;
            }

            if (q.path.length != 2) {
                alert('Укажите объект и атрубут!');
                return;
            }

            if (index === undefined)
                Base().addQuestion(q.text, q.path);
            else
                Base().editQuestion(index, q.text, q.path);

            updateQuestions();
        });

        var declineBtn = $('<button/>').addClass('icon decline').click(function () {
            updateQuestions();
        });

        result.append(text).append(selects).append(acceptBtn).append(declineBtn);
        return result;
    }

});