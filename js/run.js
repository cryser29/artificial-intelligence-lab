$(function () {
    var root = $('.questions-container');

    var questions = Base().getQuestions().map(function (q) {
        return $.extend({}, q, {
            answers: Base().getObjects(q.path)
        });
    });

    askQuestion(0);

    function askQuestion(number) {
        if (number > questions.length - 1) {
            showResult();
            return;
        }

        var question = questions[number];

        var container = $('<div/>').addClass('question')
            .append($('<span/>').text(question.text));

        question.answers.forEach(function (answer) {
            var button = $('<button/>').addClass('extra').text(answer).click(function () {
                question.answer = answer;
                askQuestion(number + 1);
            });
            container.append(button);
        });

        root.empty().append(container);
    }

    function showResult() {
        var info = [];
        questions.forEach(function (q) {
            info.push([].concat(q.path).concat(q.answer));
        });

        var rules = Base().getRules();

        while (true) {
            var find = false;

            rules.forEach(function (rule) {
                if (info.some(function (el) {
                        return isEqual(el, rule.conclusion);
                    })) {
                    return;
                }

                var callback = function (condition) {
                    return info.some(function (el) {
                        return isEqual(el, condition);
                    });
                };
                if (rule.logical == 'and' ? rule.conditions.every(callback) : rule.conditions.some(callback)) {
                    info.push(rule.conclusion);
                    find = true;
                }
            });

            if (!find) break;
        }

        if (info.length > questions.length) {
            info.splice(0, questions.length);
            info = info.map(function (el) {
                return el.join(' - ');
            });
            var text = 'Результат в соответствии с Вашими ответами: ' + info.join(', ');
        } else {
            text = 'Не нашлось ни одного объекта, удовлетворяющего Вашим ответам';
        }
        root.empty().append(
            $('<div/>').addClass('result')
                .append($('<span/>').text(text))
                .append($('<button/>').addClass('primary').text('Заново').click(function () {askQuestion(0);}))
        );
    }

    function isEqual(a, b) {
        if (a.length != b.length)
            return false;

        var result = true;
        a.forEach(function (el, i) {
            if (el != b[i]) {
                return result = false;
            }
        });
        return result;
    }
});
