(function ($)
{
    $.fn.mouseReaction = function (options)
    {
        var opts = $.extend({}, $.fn.mouseReaction.defaults, options);

        var prefix = opts.prefix ? opts.prefix + '-' : '';
        var hover = prefix + 'hover';
        var click = prefix + 'click';

        $(this).hover(function ()
        {
            $(this).addClass(hover);
        }, function ()
        {
            $(this).removeClass(hover).removeClass(click);
        }).on('mousedown mouseup', function ()
        {
            $(this).toggleClass(click);
        });

        return this;
    };

    $.fn.mouseReaction.defaults = {
        prefix: ''
    };

}(jQuery));