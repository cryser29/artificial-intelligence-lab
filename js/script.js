$(function () {
    var currentTab = 0;

    function updateCurrentTab() {
        $('.navigation > .item').removeClass('current').eq(currentTab).addClass('current');
        $('.content > .tab').removeClass('visible').eq(currentTab).addClass('visible');
    }

    updateCurrentTab();

    $('.navigation > .item').click(function () {
        var index = $(this).index();
        if (index == currentTab)
            return;
        currentTab = index;
        updateCurrentTab();
    });

    $('.navigation > .play').click(function () {
        window.open('/play.html', 'Play', 'width=600,height=400');
    });
});
