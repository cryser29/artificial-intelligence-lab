function createObjectSelect(path, value, maxPathLength) {
    if (maxPathLength === undefined)
        maxPathLength = 1;

    var select = $('<select/>');
    var keys = Base().getObjects(path);
    select.append($('<option/>'));
    for (var i = 0; i < keys.length; i++) {
        var option = $('<option/>').text(keys[i]).attr('value', keys[i]);
        select.append(option);
    }
    if (value)
        select.val(value);
    select.change(function () {
        if (path.length > maxPathLength - 1)
            return;

        $(this).nextAll('select').remove();
        var newPath = path.slice(0);
        newPath.push($(this).val());
        var newSelect = createObjectSelect(newPath, null, maxPathLength);
        $(this).after(newSelect);
    });
    return select;
}

function stringifyPath(path) {
    var result = '';
    for (var i = 0; i < path.length; i++) {
        result += path[i];
        if (i != path.length - 1)
            result += ' - ';
    }
    return result;
}