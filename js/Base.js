var Base = (function () {
    var instance;

    return function Construct_base() {
        if (instance) {
            return instance;
        }
        if (this && this.constructor === Construct_base) {
            instance = this;

            var data;
            var key = 'ai_base';

            init();

            // Objects
            this.addObject = function (object, path) {
                if (!object)
                    return {error: 'Нельзя создать пустой %%!'};

                var node = getObjectsNode(path);

                if (node[object])
                    return {error: 'Такой %% уже существует!'};

                node[object] = {};
                save();

                return {};
            };

            this.getObjects = function (path) {
                return Object.keys(getObjectsNode(path));
            };

            this.deleteObject = function (object, path) {
                var node = getObjectsNode(path);
                delete node[object];
                save();
            };

            // Questions
            this.addQuestion = function (text, path) {
                data.questions.push({text: text, path: path});
                save();
            };

            this.getQuestions = function () {
                return data.questions;
            };

            this.deleteQuestion = function (index) {
                data.questions.splice(index, 1);
                save();
            };

            this.editQuestion = function (index, text, path) {
                data.questions[index] = {text: text, path: path};
                save();
            };

            // Rules
            this.addRule = function (rule) {
                data.rules.push(rule);
                save();
            };

            this.getRules = function () {
                return data.rules;
            };

            this.deleteRule = function (index) {
                data.rules.splice(index, 1);
                save();
            };

            this.editRule = function (index, rule) {
                data.rules[index] = rule;
                save();
            };

            function init() {
                data = $.parseJSON(localStorage.getItem(key));
                if (!data) {
                    if (confirm('Загрузить пример по умолчанию?')) {
                        $.getJSON('/default.json', function (json) {
                            data = json;
                            save();
                            location.reload();
                        });
                    } else {
                        data = {objects: {}, questions: [], rules: []};
                        save();
                    }
                }
            }

            function save() {
                localStorage.setItem(key, JSON.stringify(data));
            }

            function getObjectsNode(path) {
                var node = data.objects;
                if (!path)
                    return node;
                $.each(path, function (i, key) {
                    node = node[key];
                });
                return node;
            }

        } else {
            return new Construct_base();
        }
    };
}());