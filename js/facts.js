$(function () {

    var root = $('.tab.facts');
    var current = [];
    var names = ['объект', 'атрибут', 'значение'];

    var objects = root.find('.column');
    var objectsAddBtns = objects.find('.header > .add');
    var objectsItems = objects.find('.items');

    updateObjects(0);

    objectsAddBtns.click(function () {
        var column = $(this).parent().parent();
        if (!column.hasClass('active'))
            return;
        var index = column.index();
        var name = names[index];
        var key = prompt('Введите ' + name);
        if (!key)
            return;
        var res = Base().addObject(key, current.slice(0, index));
        if (res.error)
            alert(res.error.replace('%%', name));
        else {
            updateObjects(index);
            openObject(index, key);
        }
    });

    function updateObjects(index) {
        var column = objects.eq(index);
        var container = objectsItems.eq(index);
        container.empty();
        column.removeClass('active');

        if (index > current.length)
            return;

        column.addClass('active');

        var path = [];
        for (var i = 0; i < index; i++) {
            path.push(current[i]);
        }

        $.each(Base().getObjects(path), function (i, object) {
            var item = $('<div/>').text(object).attr('data-key', object);
            item.click(function () {
                openObject(index, object);
            });
            var deleteBtn = $('<div/>').addClass('delete').click(function (e) {
                e.stopPropagation();
                if (confirm('Вы уверены, что хотите удалить ' + names[index] + ' "' + object + '"?')) {
                    Base().deleteObject(object, path);
                    current.splice(index);
                    for (var i = index; i < names.length; i++) {
                        updateObjects(i);
                    }
                }
            });
            item.append(deleteBtn);
            container.append(item);
        });
    }

    function openObject(index, key) {
        if (index == names.length - 1)
            return;

        objectsItems.eq(index)
            .children().removeClass('current').end()
            .find('[data-key="' + key + '"]').addClass('current');

        current[index] = key;
        current.splice(index + 1);

        for (var i = index + 1; i < names.length; i++) {
            updateObjects(i);
        }
    }
});