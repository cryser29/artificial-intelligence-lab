$(function () {
    var root = $('.tab.rules > .column');
    var addBtn = root.find('.header > .add');
    var container = root.find('.items');

    addBtn.click(function () {
        var view = createEditableView();
        container.append(view);
    });

    updateRules();

    function updateRules() {
        container.empty();
        $.each(Base().getRules(), function (i, r) {
            container.append(createStaticView(r, i));
        });
    }

    function createStaticView(r, index) {
        var deleteBtn = $('<div/>').addClass('delete').click(function () {
            if (confirm('Вы уверены, что хотите удалить правило?')) {
                Base().deleteRule(index);
                updateRules();
            }
        });

        var editBtn = $('<div/>').addClass('edit').click(function () {
            result.replaceWith(createEditableView(r, index));
        });

        var string = 'ЕСЛИ ';
        $.each(r.conditions, function (i, condition) {
            string += stringifyPath(condition) + ' ';
            if (i != r.conditions.length - 1)
                string += logicalTitle(r.logical);
        });
        string += 'ТО ' + stringifyPath(r.conclusion);

        var result = $('<div/>').addClass('rule-static')
            .append($('<span/>').text(string))
            .append(deleteBtn)
            .append(editBtn);

        return result;
    }

    function createEditableView(r, index) {
        var result = $('<div/>').addClass('editable');

        var orRadio = $('<input>').attr('type', 'radio').attr('name', 'logical').attr('value', 'or');
        result.append(orRadio).append(' ИЛИ ');
        var andRadio = $('<input>').attr('type', 'radio').attr('name', 'logical').attr('value', 'and');
        result.append(andRadio).append(' И ');

        result.find('[name=logical]').change(function () {
            var value = $(this).val();
            result.find('.logical').text(logicalTitle(value)).removeClass('or and').addClass(value);
        });

        var logical = r && r.logical ? r.logical : 'or';
        result.find('[name=logical][value=' + logical + ']').prop('checked', true);

        result.append($('<div/>').text('ЕСЛИ'));

        var selects = [];
        if (r && r.conditions) {
            $.each(r.conditions, function (i, condition) {
                var array = [];
                for (var j = 0; j < condition.length; j++) {
                    array.push(createObjectSelect(condition.slice(0, j), condition[j], 2));
                }
                selects.push(array);
            });
        }
        else
            selects.push([createObjectSelect([], null, 2)]);
        $.each(selects, function (i, select) {
            var view = $('<div/>').addClass('condition-container');
            $.each(select, function (j, s) {
                view.append(s);
            });
            result.append(view);
        });

        function updateRemoveBtn() {
            removeBtn.prop('disabled', result.find('.condition-container').length == 1);
        }

        var removeBtn = $('<button/>').addClass('extra').text('Удалить').click(function () {
            result.find('.condition-container').last().remove();
            updateRemoveBtn();
        });
        updateRemoveBtn();
        result.append(removeBtn);

        var addBtn = $('<button/>').addClass('primary').text('Добавить').click(function () {
            var container = $('<div/>').addClass('condition-container');

            var logicalValue = result.find('[name=logical]:checked').val();
            var logicalSpan = $('<span/>').addClass('logical').text(logicalTitle(logicalValue)).addClass(logicalValue);
            container.append(logicalSpan);

            container.append(createObjectSelect([], null, 2));

            container.insertBefore(removeBtn);

            updateRemoveBtn();
        });
        result.append(addBtn);

        var then = $('<div/>').addClass('then').text('ТО');
        result.append(then);

        var conclusion = $('<div/>').addClass('conclusion-container');
        if (r && r.conclusion) {
            for (var j = 0; j < r.conclusion.length; j++) {
                conclusion.append(createObjectSelect(r.conclusion.slice(0, j), r.conclusion[j], 2));
            }
        }
        else
            conclusion.append([createObjectSelect([], null, 2)]);
        result.append(conclusion);

        var acceptBtn = $('<button/>').addClass('icon accept').click(function () {
            var error = false;
            var obj = {conditions: []};
            obj.logical = result.find('[name=logical]:checked').val();

            $.each(result.find('.condition-container'), function (i, container) {
                var path = parseSelects($(container).find('select'), function (error) {
                    switch (error) {
                        case 'incomplete-condition':
                            alert('Неполное условие #' + (i + 1));
                            break;
                        case 'not-selected-value':
                            alert('Не выбрано значение в условии #' + (i + 1));
                            break;
                    }
                });
                if (path)
                    obj.conditions.push(path);
                else {
                    error = true;
                    return false;
                }
            });

            if (error)
                return;

            var path = parseSelects(result.find('.conclusion-container > select'), function (error) {
                switch (error) {
                    case 'incomplete-condition':
                        alert('Неполное заключение');
                        break;
                    case 'not-selected-value':
                        alert('Не выбрано значение в заключении');
                        break;
                }
            });
            if (path)
                obj.conclusion = path;
            else
                return;

            if (index === undefined)
                Base().addRule(obj);
            else
                Base().editRule(index, obj);

            updateRules();
        });
        result.append(acceptBtn);

        var declineBtn = $('<button/>').addClass('icon decline').click(function () {
            updateRules();
        });
        result.append(declineBtn);

        return result;
    }

    function logicalTitle(value) {
        switch (value) {
            case 'or':
                return 'ИЛИ ';
            case 'and':
                return 'И ';
        }
    }

    function parseSelects(selects, error) {
        if (selects.length != 3) {
            error('incomplete-condition');
            return false;
        }
        var e = false;
        var path = [];
        $.each(selects, function (j, select) {
            var value = $(select).val();
            if (value)
                path.push(value);
            else {
                error('not-selected-value');
                e = true;
                return false;
            }
        });
        if (e)
            return false;
        else
            return path;
    }

});